# FCOOL for iDPP@CLEF 2022  #

This repository contains the runs, resources, and code, of the `FCOOL`, Faculty of Sciences of the
University of Lisbon, Portugal, participating in the [iDPP@CLEF 2022](https://brainteaser.health/open-evaluation-challenges/idpp-2022/)
community effort. 

### Organisation of the repository ###

The repository is organised as follows:

* `submission`: this folder contains the runs submitted for the different tasks.
* `score`: this folder contains the performance scores of the submitted runs.

iDPP@CLEF 2022 consists of *two tasks* 

* **Task 1** - Ranking Risk of Impairment
* **Task 2** - Predicting Time of Impairment

Therefore, the `submission` and `score` folders are organized into sub-folders for each task.

### Reference ###

For an explanation of the developed approaches and for citing them, please, refer to:

Branco, R., Soares, D., Martins, A. S., Auletta, E., Castanho, E. N., Nunes, S., Serrano, F., Sousa, R. T., Pesquita, C., Madeira, S. C., and Aidos, H. (2022). Hierarchical Modelling for ALS Prognosis: Predicting the Pro- gression Towards Critical Events. In Faggioli, G., Ferro, N., Hanbury, A., and Potthast, M., editors, _CLEF 2022 Working Notes_, pages 1211– 1227. CEUR Workshop Proceedings (CEUR-WS.org), ISSN 1613-0073. [http://ceur-ws.org/Vol-3180/](http://ceur-ws.org/Vol-3180/).

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

